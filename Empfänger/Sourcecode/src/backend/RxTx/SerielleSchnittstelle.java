package backend.RxTx;

import com.fazecast.jSerialComm.*;

import java.io.IOException;

interface IO {
    void read(byte read);

    void written(byte[] out);
}

public abstract class SerielleSchnittstelle implements IO {

    private SerialPort port;

    public SerielleSchnittstelle(String port) {
        start(port);
    }

    private void start() {
        port.setBaudRate(9600);
        if (!port.isOpen()) {
            if (!port.openPort()) {
                System.err.println("Error : cant open Port " + port.getSystemPortName());
            }
        }

        Thread t = new Thread(() -> {
            if (port.isOpen()) {
                final int len = 10;
                while (true) {
                    port.setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 0, 0); //dont time out
                    byte[] cbuffer = new byte[len];
                    for (int i = 0; i < len; i++) {
                        cbuffer[i] = -1;
                    }
                    if (port.bytesAvailable() > 0) {
                        port.readBytes(cbuffer, len);
                        for (int i = 0; i < len; i++) {
                            if (cbuffer[i] > -1)
                                read(cbuffer[i]);//call methode once a byte is read
                        }
                    }
                }
            }
        });
        t.start();
    }

    private void start(String p) {
        port = SerialPort.getCommPort(p);
        if (!port.getSystemPortName().equals(p)) {
            System.err.print("Port ");
            System.out.print(p);
            System.err.println(" not found or busy");
            System.exit(0);
        }
        start();
    }

    public void write(byte out) throws IOException {
        byte[] outArray = new byte[1];
        outArray[0] = out;
        write(outArray);
    }

    public void write(byte[] out) throws IOException {
        if (port.isOpen()) {
            port.getOutputStream().write(out, 0, out.length);
            port.getOutputStream().flush();
            written(out);
        } else {
            System.err.println("Error : Port " + port.getSystemPortName() + " not found or busy");
            throw new IOException();
        }
    }
}