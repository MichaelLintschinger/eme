package backend.db;

import backend.Constants;

import java.sql.*;
import java.util.Scanner;

public class RecreateDB {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String USER, PASS, DB_URL = Constants.db_url;
        System.out.print("user: ");
        USER = s.next();
        System.out.print("password (type '-' if there is no password): ");
        PASS = s.next();

        Connection conn = null;
        Statement stmt = null;
        try {


            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, (PASS.equals("-") ? null : PASS));

            //STEP 4: Execute a query
            System.out.println("deleting Database...");
            stmt = conn.createStatement();

            String sql = "drop database if exists " + Constants.db_name + ";";
            stmt.executeUpdate(sql);
            sql = "CREATE DATABASE " + Constants.db_name + ";";
            stmt.executeUpdate(sql);
            try {
                sql = "CREATE USER '" + Constants.username + "'@'localhost' IDENTIFIED BY '" + Constants.password + "';";
                stmt.executeUpdate(sql);
            } catch (SQLException seee) {

            }
            if (!USER.equals(Constants.username)) {
                sql = "grant all privileges on `" + Constants.db_name + "`.* to '" + Constants.username + "'@'localhost' with grant option ;";
                stmt.executeUpdate(sql);
            }
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Recreating DB");
        SQLmanager SQLmanager = new SQLmanager(DB_URL + "eme", Constants.username, Constants.password);
        SQLmanager.execute("drop table if exists " + Constants.db_name_comm + ";");
        SQLmanager.execute("drop table if exists " + Constants.db_name_int + ";");
        SQLmanager.execute("create table " + Constants.db_name_comm + "(" +
                Constants.db_comm_c0 + " bigint primary key auto_increment," +
                Constants.db_comm_c1 + " datetime not null," +
                Constants.db_comm_c2 + " binary(1) not null," +
                Constants.db_comm_c3 + " boolean," +
                Constants.db_comm_c4 + " boolean);");
        SQLmanager.execute("create table " + Constants.db_name_int + "(" +
                Constants.db_int_c0 + " boolean, " +
                Constants.db_int_c1 + " bigint not null);");
        SQLmanager.execute("insert into " + Constants.db_name_int + "(" + Constants.db_int_c0 + "," + Constants.db_int_c1 + ") values(0, 0);");
        SQLmanager.execute("create table " + Constants.db_name_measure_packages + "(\n" +
                Constants.db_meas_p_c0 + " int not null primary key auto_increment,\n" +
                Constants.db_meas_p_c1 + " datetime\n" +
                ");");
        SQLmanager.execute("create table " + Constants.db_name_measure + "(\n" +
                Constants.db_meas_c0 + " int not null,\n" +
                Constants.db_meas_c1 + " int not null,\n" +
                Constants.db_meas_c2 + " double,\n" +
                "primary key (" + Constants.db_meas_c0 + "," + Constants.db_meas_c1 + "),\n" +
                "foreign key(" + Constants.db_meas_c0 + ") references " + Constants.db_name_measure_packages + "(" + Constants.db_meas_p_c0 + ")\n" +
                ");\n"
        );
        System.out.println("done");
    }
}
