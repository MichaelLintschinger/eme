package backend.db;

import backend.Constants;

import java.sql.*;
import java.time.*;

public class SQLmanager {
    String url, name, password, stringOut = "";
    long index;
    boolean out_b = false;

    public SQLmanager(String url, String name, String password) {
        this.url = url;
        this.password = password;
        this.name = name;
    }
    public void readIndex() {
        executeAndResult("select " + Constants.db_int_c1 + " from " + Constants.db_name_int + ";", new ProcessResultSet() {
            @Override
            public void processResultSet(ResultSet rs) throws SQLException {
                index = rs.next() ? rs.getLong(Constants.db_int_c1) : 1;
            }
        });

    }

    public long getIndex() {
        return index;
    }

    public void execute(String sql) {
        executeAndResult(sql, null);
    }

    public void executeAndResult(String sql, ProcessResultSet do_after) {
        boolean update = do_after == null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, name, password);
            stmt = conn.createStatement();
            if (update) {
                stmt.executeUpdate(sql);
            } else {
                rs = stmt.executeQuery(sql);
                if (do_after != null)
                    do_after.processResultSet(rs);
            }
            stmt.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException ignored) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    //Interrupt
    public boolean hasNextBytes() {
        out_b = false;
        String sql = "select " + Constants.db_int_c0 + " from " + Constants.db_name_int + ";";
        executeAndResult(sql, new ProcessResultSet() {
            @Override
            public void processResultSet(ResultSet rs) throws SQLException {
                boolean tr = false;
                while (rs.next()) {//create ArrayList from DB
                    tr = true;
                    out_b = rs.getBoolean(Constants.db_int_c0);
                }
                if (!tr)
                    executeAndResult("insert into " + Constants.db_name_int + "(" + Constants.db_int_c0 + ", " + Constants.db_int_c1 + ") values (1,0);", null);
            }
        });
        return out_b;
    }

    public void updateInterrupt(boolean tr) {
        String sql = "update " + Constants.db_name_int + " set " + Constants.db_int_c0 + " = " + (tr ? 1 : 0) + ";";
        executeAndResult(sql, null);
    }

    public String getAllCommands(long index) {
        stringOut = "";
        if (index == -2) {
            index = this.index - 1000;
            index = (index < 0) ? 0 : index;
        }
        index = (index < 0) ? this.index : index;
        String sql = "select " + Constants.db_comm_c2 + " from " + Constants.db_name_comm + " where " + Constants.db_comm_c0 + " >= " + index + ";";
        executeAndResult(sql, new ProcessResultSet() {
            @Override
            public void processResultSet(ResultSet rs) throws SQLException {
                while (rs.next()) {
                    stringOut += (char) rs.getBytes(Constants.db_comm_c2)[0];
                }
            }
        });

        return stringOut;
    }

    public void write(LocalDateTime date, byte value, boolean sent, boolean simulated) {
        String sql = "insert into " + Constants.db_name_comm + " (" + Constants.db_comm_c1 + ", " + Constants.db_comm_c2 + ", " + Constants.db_comm_c3 + ", " + Constants.db_comm_c4 + ") values ('" + date + "', '" + (char) value + "', " + (sent ? 1 : 0) + ", " + (simulated ? 1 : 0) + ");";
        executeAndResult(sql, null);
    }

}
