package backend.db;

import java.time.LocalDateTime;

public class DBEntry {
    LocalDateTime time;
    byte received;
    boolean sent,simulated;
    long id;

    public boolean isSimulated() {
        return simulated;
    }

    public void setSimulated(boolean simulated) {
        this.simulated = simulated;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public byte getReceived() {
        return received;
    }

    public void setReceived(byte received) {
        this.received = received;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String toString() {
        return "ID: " + getId() + "\ndate : " + getTime() + "\nsent : " + isSent() + "\nreceived : " + (char) received;
    }
}
