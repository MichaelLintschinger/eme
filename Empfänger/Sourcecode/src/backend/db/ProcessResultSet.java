package backend.db;

import java.sql.*;

interface prs{
     void processResultSet(ResultSet rs) throws SQLException;
}


public abstract class ProcessResultSet implements prs {}