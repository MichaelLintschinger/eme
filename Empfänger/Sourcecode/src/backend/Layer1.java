package backend;

import backend.db.*;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Layer1 {
    static SQLmanager SQLmanager;
    static boolean newData = false;
    static String buffer = "";
    static int index = 0, errorcounter = 0;

    public static void main(String[] args) {
        new Thread(() -> {//reset counter
            try {
                Thread.sleep(1000);
                errorcounter = 0;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        SQLmanager = new SQLmanager(Constants.db_url + Constants.db_name, Constants.username, Constants.password);
        while (true) {
            try {
                //set index
                SQLmanager.executeAndResult("select * from " + Constants.db_name_int + ";", new ProcessResultSet() {
                    @Override
                    public void processResultSet(ResultSet rs) throws SQLException {
                        while (rs.next()) if (rs.isLast()) {
                            newData = rs.getBoolean(Constants.db_int_c0);
                            index = rs.getInt(Constants.db_int_c1);
                        }
                    }
                });

                //gather data

                if (newData || buffer.length() > 0) {
                    SQLmanager.executeAndResult("select " + Constants.db_comm_c2 + "," + Constants.db_comm_c0 + " from " + Constants.db_name_comm + " where " + Constants.db_comm_c0 + ">=" + index + " and " + Constants.db_comm_c3 + "=0;", new ProcessResultSet() {
                        @Override
                        public void processResultSet(ResultSet rs) throws SQLException {
                            while (rs.next()) {
                                buffer += rs.getString(Constants.db_comm_c2);
                                id = rs.getInt(Constants.db_comm_c0);
                            }
                        }
                    });
                    id++;
                    SQLmanager.executeAndResult("update " + Constants.db_name_int + " set " + Constants.db_int_c1 + "=" + id + "," + Constants.db_int_c0 + "=0;", null);

                    //Process data
                    String[] commands = commandsFromBuffer();
                    for (String command : commands) {
                        try {
                            if (("" + (char) Constants.OK + (char) Constants.get_data).equals(command.substring(0, 2))) {
                                double[] adc = new double[command.substring(2).split("" + (char) Constants.break_character).length];
                                for (int i = 0; i < adc.length; i++) {
                                    adc[i] = Double.parseDouble(command.substring(2).split("" + (char) Constants.break_character)[i]);
                                }
                                System.err.println(command + "; " + adc[0]);
                                addNewADC(adc);
                            } else if (!(("" + (char) Constants.OK + (char) Constants.check_connection).equals(command.substring(0, 2)) ||
                                    ("" + (char) Constants.OK + (char) Constants.check_EEPROM).equals(command.substring(0, 2)) ||
                                    ("" + (char) Constants.OK + (char) Constants.check_firmware_version).equals(command.substring(0, 2)) ||
                                    ("" + (char) Constants.OK + (char) Constants.check_chipID).equals(command.substring(0, 2)) ||
                                    ("" + (char) Constants.OK + (char) Constants.get_data_amount).equals(command.substring(0, 2)) ||
                                    ("" + (char) Constants.OK + (char) Constants.delete_data).equals(command.substring(0, 2)))) {
                                continue;
                            }
                            buffer = buffer.replaceFirst(command + (char) Constants.end_of_command, "");
                        } catch (Exception e) {
                            e.printStackTrace();
                            errorcounter++;
                            if (errorcounter > 5) {
                                while (buffer.toCharArray()[0] != (char) Constants.end_of_command)
                                    buffer = buffer.substring(1);
                                buffer = buffer.substring(1);
                                errorcounter = 0;
                            }
                        }
                    }
                }

                Thread.sleep(500);
            } catch (Exception ignored) {
            }
        }
    }

    static String[] commandsFromBuffer() {
        ArrayList<String> arrayList = new ArrayList<>();
        String s = "";
        for (char c : buffer.toCharArray()) {
            if (c == (char) Constants.end_of_command) {
                arrayList.add(s);
                s = "";
            } else {
                s += c;
            }
        }
        String[] strings = new String[arrayList.size()];
        for (int i = 0; i < strings.length; i++) {
            strings[i] = arrayList.get(i);
        }
        return strings;
    }

    static int id = 0;

    static synchronized void addNewADC(double[] adc) {
        LocalDateTime dateTime = LocalDateTime.now();
        SQLmanager.executeAndResult("insert into " + Constants.db_name_measure_packages + "(" + Constants.db_meas_p_c1 + ") values ('" + dateTime + "');", null);

        SQLmanager.executeAndResult("select " + Constants.db_meas_p_c0 + " from " + Constants.db_name_measure_packages + ";", new ProcessResultSet() {
            @Override
            public void processResultSet(ResultSet rs) throws SQLException {
                while (rs.next()) {
                    if (rs.isLast()) {
                        id = rs.getInt(Constants.db_meas_p_c0);
                    }
                }
            }
        });

        for (int i = 0; i < adc.length; i++) {
            SQLmanager.executeAndResult("insert into " + Constants.db_name_measure + "(" + Constants.db_meas_c0 + "," + Constants.db_meas_c1 + "," + Constants.db_meas_c2 + ") values (" + id + "," + i + "," + adc[i] + ");", null);
        }
    }
}
