package backend;

import backend.db.*;
import frontend.ringbuffer.*;

import java.sql.*;
import java.util.ArrayList;

public abstract class DataContainer {
    private static boolean live = true;
    private static ArrayList<Segment> segments;
    private static SQLmanager SQLmanager = new SQLmanager(Constants.db_url + Constants.db_name, Constants.username, Constants.password);

    public static ArrayList<Segment> getSegments() {
        if (segments == null) segments = new ArrayList<Segment>();
        return segments;
    }

    public static Segment getSegment(int i) {
        try {
            return getSegments().get(i);
        } catch (Exception e) {
            getSegments().add(new Segment());
            return getSegment(i);
        }
    }

    public static void clearSegments() {
        segments.clear();
    }

    public static void loadSegments(String start, String stop) {
        String sql;
        if (start == null) {
            sql = "select * from " + Constants.db_name_measure + ";";
            SQLmanager.executeAndResult(sql, new ProcessResultSet() {
                @Override
                public void processResultSet(ResultSet rs) throws SQLException {
                    while (rs.next()) {
                        setValue(rs.getInt(Constants.db_meas_c0), rs.getInt(Constants.db_meas_c1), rs.getDouble(Constants.db_meas_c2));
                    }
                }
            });
        } else { // /load 2020-01-31 11:37:42 2020-01-31 11:37:42
            stop = (stop == null) ? start : stop;
            sql = "select * from " + Constants.db_name_measure + " INNER JOIN " + Constants.db_name_measure_packages + " MP on " + Constants.db_name_measure + "." + Constants.db_meas_c0 + " = MP." + Constants.db_meas_p_c0 + " where MP." + Constants.db_meas_p_c1 + ">='" + start + "' and MP." + Constants.db_meas_p_c1 + " <= '" + stop + "';";
            System.err.println(sql);
            SQLmanager.executeAndResult(sql, new ProcessResultSet() {
                @Override
                public void processResultSet(ResultSet rs) throws SQLException {
                    while (rs.next()) {
                        setValue(rs.getInt(Constants.db_meas_c0), rs.getInt(Constants.db_meas_c1), rs.getDouble(Constants.db_meas_c2));
                    }
                }
            });
        }
        setLive(false);
        System.out.println(tostring());
    }

    public static void setSegment(Segment segment) {
        int i = segment.getIndex();
        if (i < 0) return;
        while (i >= getSegments().size()) {
            getSegments().add(new Segment());
        }
        getSegments().set(i, segment);
    }

    public static void setValue(int indexSegment, int indexADC, double value) {

        getSegment(indexSegment).setAdc(indexADC, value);
    }

    public static boolean isLive() {
        return live;
    }

    public static void setLive(boolean live) {
        DataContainer.live = live;
    }

    public static String tostring() {
        String out = "";
        for (Segment s : getSegments()) {
            out += s.toString() + "\n";
        }
        return out;
    }
}
