package backend;

import java.io.*;

public class Logger {
    private static boolean started = false;
    private static BufferedWriter writer;

    public static synchronized void append(String add) {
        if (!started) try {
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            writer.write(add);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void start() throws IOException {
        File file = new File(Constants.logger_Path);
        file.mkdir();
        writer = new BufferedWriter(new FileWriter(Constants.logger_Path + Constants.logger_file, true));
        started = true;
    }

    public static void close() {
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
