package frontend;

import backend.db.*;
import backend.Constants;

import javax.swing.*;
import java.awt.*;
import java.sql.*;

public class StatusPanel extends JPanel {
    private JLabel p_connection, p_eeprom, p_chipID, p_firmwareVersion, l_lastReceived, l_visual;
    private SQLmanager sqlmanager = new SQLmanager(Constants.db_url + Constants.db_name, Constants.username, Constants.password);
    private final int margin = 7;
    private String firmware_v, chip_ID;

    public StatusPanel() {
        setLayout(null);
        add(getP_connection());
        add(getP_eeprom());
        add(getP_chipID());
        add(getP_firmwareVersion());
        add(getL_lastReceived());
        add(getL_visual());
        checksFromDB(sqlmanager.getAllCommands(-2));
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(1000);
                    checksFromDB(sqlmanager.getAllCommands(-2));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        getP_chipID();
        getP_connection();
        getP_eeprom();
        getP_firmwareVersion();
        getL_lastReceived();
        getL_visual();
    }

    public JLabel getP_connection() {
        if (p_connection == null) {
            p_connection = new JLabel("connection stable : ", JLabel.CENTER);
            p_connection.setOpaque(true);
            p_connection.setBackground(Color.lightGray);
            p_connection.setBorder(BorderFactory.createLineBorder(Color.black));
        }
        p_connection.setBounds(margin, margin + (getHeight() - 2 * margin) / 3, getWidth() / 2 - margin + 1, (getHeight() - 2 * margin) / 3 + 1);
        return p_connection;
    }

    public JLabel getP_eeprom() {
        if (p_eeprom == null) {
            p_eeprom = new JLabel("EEPROM", JLabel.CENTER);
            p_eeprom.setOpaque(true);
            p_eeprom.setBackground(Color.green);
            p_eeprom.setBorder(BorderFactory.createLineBorder(Color.black));
        }
        p_eeprom.setBounds(getWidth() / 2, margin + (getHeight() - 2 * margin) / 3, getWidth() / 2 - margin, (getHeight() - 2 * margin) / 3 + 1);
        return p_eeprom;
    }

    public JLabel getP_chipID() {
        if (p_chipID == null) {
            p_chipID = new JLabel("Chip ID", JLabel.CENTER);
            p_chipID.setOpaque(true);
            p_chipID.setBackground(Color.red);
            p_chipID.setBorder(BorderFactory.createLineBorder(Color.black));
        }
        p_chipID.setText("Chip ID : " + chip_ID);
        p_chipID.setBounds(margin, margin + 2 * (getHeight() - 2 * margin) / 3, getWidth() / 2 - margin + 1, (getHeight() - 2 * margin) / 3);
        return p_chipID;
    }

    public JLabel getP_firmwareVersion() {
        if (p_firmwareVersion == null) {
            p_firmwareVersion = new JLabel("Firmwareversion", JLabel.CENTER);
            p_firmwareVersion.setOpaque(true);
            p_firmwareVersion.setBackground(Color.black);
            p_firmwareVersion.setBorder(BorderFactory.createLineBorder(Color.black));
        }
        p_firmwareVersion.setText("Firmwareversion : " + firmware_v);
        p_firmwareVersion.setBounds(getWidth() / 2, margin + 2 * (getHeight() - 2 * margin) / 3, getWidth() / 2 - margin, (getHeight() - 2 * margin) / 3);
        return p_firmwareVersion;
    }
    String stringOut;
    public JLabel getL_lastReceived() {
        if (l_lastReceived == null) {
            l_lastReceived = new JLabel();
        }
        l_lastReceived.setBounds(20, 10, getWidth() / 2 - 20, getHeight() / 3 - 10);
        stringOut="no data";
        sqlmanager.executeAndResult("select " + Constants.db_comm_c1 + " from " + Constants.db_name_comm + " where " + Constants.db_comm_c3 + "='0';", new ProcessResultSet() {
            @Override
            public void processResultSet(ResultSet rs) throws SQLException {
                while (rs.next()) {
                    if (rs.isLast()) {
                        stringOut = rs.getString(Constants.db_comm_c1);
                    }
                }
            }
        });

        stringOut=stringOut.replace('-','.');

        if (stringOut.length() > 50) return l_lastReceived;
        if (stringOut.split("\\.").length > 1)
            stringOut = "<html>Last bit received:<br>" + stringOut.split("\\.")[2].split(" ")[0] + "." + stringOut.split("\\.")[1] + "." + stringOut.split("\\.")[0] + "<br><font color=\"blue\">" + stringOut.split(" ")[1] + "</font>";
        l_lastReceived.setText(stringOut);
        return l_lastReceived;
    }

    public void checksFromDB(String inputString) {//check for status panel
        checksFromDB();
        String[] commands = inputString.split("" + (char) Constants.end_of_command);

        for (int i = 0; i < commands.length; i++) {
            try {
                if (commands[i].substring(0, 2).contains("" + (char) Constants.check_chipID)) {
                    if (commands[i].substring(0, 1).equals("" + (char) Constants.OK)) {
                        chip_ID = commands[i].substring(2);
                        p_chipID.setBackground(Color.green);
                    } else {
                        p_chipID.setBackground(Color.red);
                    }
                } else if (commands[i].substring(0, 2).contains("" + (char) Constants.check_firmware_version)) {
                    if (commands[i].substring(0, 1).equals("" + (char) Constants.OK)) {
                        firmware_v = commands[i].substring(2);
                        p_firmwareVersion.setBackground(Color.green);
                    } else {
                        p_firmwareVersion.setBackground(Color.red);
                    }
                }
            } catch (Exception e) {

            } finally {
                continue;
            }
        }
    }

    long id_check_eeprom, id_connection;

    private void checksFromDB() {
        id_check_eeprom = -1;
        id_connection = -1;

        sqlmanager.executeAndResult("SELECT * from " + Constants.db_name_comm + " where " + Constants.db_comm_c3 + " = false ; ", new ProcessResultSet() {
            @Override
            public void processResultSet(ResultSet rs) throws SQLException {
                while (rs.next()) {
                    if (rs.getString(Constants.db_comm_c2).equals("" + (char) Constants.check_EEPROM)) {
                        id_check_eeprom = rs.getLong(Constants.db_comm_c0);
                    } else if (rs.getString(Constants.db_comm_c2).equals("" + (char) Constants.check_connection)) {
                        p_connection.setText("<html>connection stable : <br><font color=blue>" + rs.getString(Constants.db_comm_c1)+"</format></html>");
                    }
                }

            }
        });
        sqlmanager.executeAndResult("SELECT * from " + Constants.db_name_comm + " where " + Constants.db_comm_c0 + " = '" + (id_check_eeprom - 1) + "';", new ProcessResultSet() {
            @Override
            public void processResultSet(ResultSet rs) throws SQLException {
                while (rs.next()) {
                    if (rs.getString(Constants.db_comm_c2).equals("" + (char) Constants.OK)) {
                        p_eeprom.setBackground(Color.green);
                    } else {
                        p_eeprom.setBackground(Color.red);
                    }
                }
            }
        });

    }

    public JLabel getL_visual() {
        if (l_visual == null) {
            l_visual = new JLabel("");
        }
        l_visual.setBounds(20 + getWidth() / 2, 0, getWidth() / 2 - margin, getHeight() / 3);
        return l_visual;
    }
}
