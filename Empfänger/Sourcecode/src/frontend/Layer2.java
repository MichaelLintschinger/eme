package frontend;

import backend.db.*;
import backend.*;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.*;

public class Layer2 {

    private static SQLmanager SQLmanager = new SQLmanager(Constants.db_url + Constants.db_name, Constants.username, Constants.password);
    static StartPagePanel startPagePanel;
    // static RingbufferPanel ringbufferPanel;
    static String buffer = "";

    public static void main(String[] args) {
        JFrame f = new JFrame("Datensammler");
        final int w = 800, h = 600;
        f.setSize(w, h);
        f.setLocationRelativeTo(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        startPagePanel = new StartPagePanel() {
            @Override
            public void refresh(long length) {
                long index = (length < 0) ? 0 : SQLmanager.getIndex() - length;
                //buffer = SQLmanager.getAllCommands(index);
            }
        };
        //ringbufferPanel = new RingbufferPanel(30, args);
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                receive_data();
                startPagePanel.repaint();
            }
        }).start();
        new Thread(() -> {
            while (true) {
                startPagePanel.getTemp_vis_panel().repaint();
                try {
                    Thread.sleep(startPagePanel.getTemp_vis_panel().isPressed() ? 50 : 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Startpage", startPagePanel);
        //tabbedPane.addTab("Ringbuffer", ringbufferPanel);
        f.add(tabbedPane);



        f.setVisible(true);

    }

    static int indexDBadc = 0;

    public static void receive_data() {
        if (SQLmanager.hasNextBytes()) {
            SQLmanager.readIndex();
            startPagePanel.getP_console().console_write("received", buffer);
        }
        if (DataContainer.isLive())
            SQLmanager.executeAndResult("select * from " + Constants.db_name_measure + " where " + Constants.db_meas_c0 + ">=" + indexDBadc + ";", new ProcessResultSet() {
                @Override
                public void processResultSet(ResultSet rs) throws SQLException {
                    while (rs.next()) {
                        if (rs.isLast())
                            indexDBadc = rs.getInt(Constants.db_meas_c0);
                        DataContainer.setValue(rs.getInt(Constants.db_meas_c0), rs.getInt(Constants.db_meas_c1), rs.getDouble(Constants.db_meas_c2));
                    }
                }
            });
    }
}
