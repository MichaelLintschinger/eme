package frontend;

import backend.*;
import backend.db.SQLmanager;
import frontend.console.Console;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

interface refresh {
    void refresh(long length);
}

public class StartPagePanel extends JPanel implements refresh {
    JPanel p_vis_navigate;
    JButton btn_vis_nav[] = new JButton[4];
    private int curveIndex = 0, dataIndex = 0;
    Console p_console;
    TempVisPanel temp_vis_panel;
    StatusPanel status_panel;
    JLabel l_panelIndex;
    SQLmanager SQLmanager = new SQLmanager(Constants.db_url + Constants.db_name, Constants.username, Constants.password);

    public StartPagePanel() {
        setLayout(null);

        p_console = new Console() {
            @Override
            public void process_command(String command) {
                super.process_command(command);
                String[] cmd = command.split(" ");
                switch (cmd[0]) {
                    case "reload": {
                        try {
                            refresh(Integer.parseInt(cmd[1]));
                        } catch (Exception e) {
                            refresh(-1);
                        }
                        break;
                    }
                    case "load": {
                        String start = cmd[1] + "T" + cmd[2], stop = null;
                        try {
                            stop = cmd[3] + "T" + cmd[4];
                        } catch (Exception e) {
                            start = cmd[1];
                            stop = cmd[2];
                        }
                        DataContainer.clearSegments();
                        DataContainer.loadSegments(start, stop);
                        break;
                    }
                    case "resume": {
                        DataContainer.clearSegments();
                        DataContainer.loadSegments(null, null);
                        DataContainer.setLive(true);
                        break;
                    }
                    case "status": {
                        System.out.println(DataContainer.tostring() + (DataContainer.isLive() ? "You are live" : "you are currently viewing older data"));
                        console_write("Help", DataContainer.tostring());
                        console_write("", DataContainer.isLive() ? "You are live" : "you are currently viewing older data");
                        break;
                    }
                    case "clear": {
                        DataContainer.clearSegments();
                        break;
                    }
                    case "setscale":
                    case "scale": {
                        try {
                            getTemp_vis_panel().setAmountData(Integer.parseInt(cmd[1]));
                        } catch (Exception ignored) {
                        }
                        break;
                    }
                    case "setoff":
                    case "setoffset":
                    case "offset": {
                        try {
                            getTemp_vis_panel().setOffsetData(Integer.parseInt(cmd[1]));
                        } catch (Exception ignored) {
                        }
                        break;
                    }
                }
            }
        };

        new Thread(() -> {
            while (true) {
                try {
                    refresh(1000);
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        p_console.addHelp("reload [length / -1] : loads either all or the latest [length] bytes from DB");
        p_console.addHelp("load [date] [date] : load all data between the two arguments (pauses live view)");
        p_console.addHelp("resume : changes back to live view");
        p_console.addHelp("status : prints current data in console");
        p_console.addHelp("setoff/setoffset/offset [int] : sets offset starting left");
        p_console.addHelp("setscale/scale [int] : sets scale/amount of data in visualisation");
        p_console.getTf_ausgabe().addTextListener(textEvent -> {
            p_console.getTf_ausgabe().setCaretPosition(p_console.getTf_ausgabe().getText().length());
        });
        add(getStatus_panel());
        add(getP_console());
        add(getL_panelIndex());
        add(getTemp_vis_panel());
        add(getP_vis_navigate());

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        getStatus_panel();
        getP_console();
        getL_panelIndex();
        getP_vis_navigate();
    }

    public Console getP_console() {
        p_console.setBounds(10, 10, getWidth() / 3 - 20, getHeight() - 20);
        return p_console;
    }

    public JPanel getP_vis_navigate() {
        if (p_vis_navigate == null) {
            p_vis_navigate = new JPanel();
            p_vis_navigate.setLayout(null);
            btn_vis_nav[0] = new JButton("/\\");
            btn_vis_nav[1] = new JButton("<");
            btn_vis_nav[2] = new JButton("\\/");
            btn_vis_nav[3] = new JButton(">");

            btn_vis_nav[0].addActionListener(e -> getTemp_vis_panel().setAmountData(getTemp_vis_panel().getAmountData() + 1));
            btn_vis_nav[1].addActionListener(e -> getTemp_vis_panel().setOffsetData(getTemp_vis_panel().getOffsetData() + 1));
            btn_vis_nav[2].addActionListener(e -> getTemp_vis_panel().setAmountData(getTemp_vis_panel().getAmountData() - 1));
            btn_vis_nav[3].addActionListener(e -> getTemp_vis_panel().setOffsetData(getTemp_vis_panel().getOffsetData() - 1));

            for (int i = 0; i < btn_vis_nav.length; i++) {
                p_vis_navigate.add(btn_vis_nav[i]);
            }

        }
        p_vis_navigate.setBounds((int) (getWidth() * 1.0 / 3 + 10), 45 + getHeight() / 3, (int) (getWidth() * 2.0 / 3 - 20), (int) ((getHeight() - 90) * 1.0 / 3 + 140) - (45 + getHeight() / 3));

        btn_vis_nav[1].setBounds(0, 0, p_vis_navigate.getWidth() / 3, p_vis_navigate.getHeight());
        btn_vis_nav[3].setBounds((int) (p_vis_navigate.getWidth() * 2.0 / 3), 0, p_vis_navigate.getWidth() / 3, p_vis_navigate.getHeight());

        btn_vis_nav[0].setBounds(p_vis_navigate.getWidth() / 3, 0, p_vis_navigate.getWidth() / 3, p_vis_navigate.getHeight() / 2);
        btn_vis_nav[2].setBounds(p_vis_navigate.getWidth() / 3, p_vis_navigate.getHeight() / 2, p_vis_navigate.getWidth() / 3, p_vis_navigate.getHeight() / 2);
        return p_vis_navigate;
    }

    public TempVisPanel getTemp_vis_panel() {
        if (temp_vis_panel == null) {
            temp_vis_panel = new TempVisPanel() {
                @Override
                public void indexChanged(int newIndex) {
                    getL_panelIndex().setText("Current Curve Index: " + newIndex);
                    curveIndex = newIndex;
                    try {
                        getStatus_panel().getL_visual().setText("<html>index : <font color=\"blue\">" + dataIndex + "</font><br>value : <font color=\"blue\">" + DataContainer.getSegments().get(dataIndex).getAdc()[curveIndex] + "</font>");
                    } catch (Exception e) {
                    }
                }

                @Override
                public void measureChanged(int index) {
                    dataIndex = index;
                    double value = -1;
                    try {
                        value = DataContainer.getSegments().get(dataIndex).getAdc()[curveIndex];
                    } catch (Exception e) {
                    }
                    try {
                        getStatus_panel().getL_visual().setText("<html>index : <font color=\"blue\">" + dataIndex + "</font><br>value : <font color=\"blue\">" + (value == -1 ? "-" : value) + "</font>");
                    } catch (Exception e) {
                    }
                }
            };
        }
        temp_vis_panel.setBounds((int) (getWidth() * 1.0 / 3 + 10), 40, (int) (getWidth() * 2.0 / 3 - 20), getHeight() / 3);
        temp_vis_panel.setFocusable(true);
        return temp_vis_panel;
    }

    public StatusPanel getStatus_panel() {
        if (status_panel == null) {
            status_panel = new StatusPanel();
            status_panel.setBorder(new TitledBorder("Status"));
        }
        status_panel.setBounds((int) (getWidth() * 1.0 / 3 + 10),
                (int) ((getHeight() - 90) * 1.0 / 3 + 140),
                (int) (getWidth() * 2.0 / 3 - 20),
                (int) ((getHeight() - 90) * 2.0 / 3 - 60));
        return status_panel;
    }

    public JLabel getL_panelIndex() {
        if (l_panelIndex == null) {
            l_panelIndex = new JLabel("Current Curve Index: 0");
        }
        l_panelIndex.setBounds((int) (getWidth() * 1.0 / 3 + 10),
                10,
                (int) (getWidth() * 2.0 / 3 - 20),
                30);
        return l_panelIndex;
    }

    @Override
    public void refresh(long length) {

    }
}
