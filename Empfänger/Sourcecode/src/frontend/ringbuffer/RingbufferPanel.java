package frontend.ringbuffer;

import backend.DataContainer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class RingbufferPanel extends JPanel {

    private int radius = 150;
    private int cellAmount = 10, delay = 1000;
    private boolean clicked = false;
    private Point mousePosition = MouseInfo.getPointerInfo().getLocation(), p_middle = new Point(0, 0);
    private RingbufferEditPanel ringbufferEditPanel;


    public RingbufferPanel(int cellAmount, String[] args) {
        if (cellAmount > 2) this.cellAmount = cellAmount;
        setLayout(null);
        for (int i = 0; i < this.cellAmount; i++) {
            Segment s = new Segment();
            s.setIndex(i);
            DataContainer.setSegment(s);
        }

        try {
            if (!args[0].equals("notest")) {
                throw new Exception();
            }
        } catch (Exception e) {
            for (int i = 0; i < cellAmount / 2; i++) {
                double[] adcs = {i * 0.1, i * 0.3};
                DataContainer.getSegments().get(i).setAdc(adcs);
                DataContainer.getSegments().get(i).setLocked(true);
            }
        }

        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                clicked = true;
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {

            }

            @Override
            public void mouseMoved(MouseEvent e) {
                mousePosition = e.getPoint();
            }
        });
        ringbufferEditPanel = new RingbufferEditPanel(150);
        add(ringbufferEditPanel);
    }

    int selectedIndexPrev = -1;

    @Override
    protected void paintComponent(Graphics g) {
        if (getWidth() < getHeight()) {
            radius = (int) (0.7 * getWidth() / 2);
        } else {
            radius = (int) (0.7 * getHeight() / 2);
        }

        p_middle = new Point(getWidth() / 2 + ringbufferEditPanel.getWidth() / 2, getHeight() / 2);
        ringbufferEditPanel.setLocation((int) (p_middle.x - radius * 1.2 - ringbufferEditPanel.getWidth()), (int) (p_middle.y - radius * 0.3));
        Graphics2D g2 = (Graphics2D) g;
        g2.setBackground(Color.white);
        g2.clearRect(0, 0, getWidth(), getHeight());

        g2.setColor(Color.BLACK);
        g2.drawOval(p_middle.x - radius, p_middle.y - radius, 2 * radius, 2 * radius);
        cellAmount = DataContainer.getSegments().size();
        for (int i = 0; i < cellAmount; i++) {
            if (!DataContainer.getSegments().get(i).isLocked()) {
                g2.setColor(Color.green);
            } else {
                g2.setColor(Color.red);
            }
            int[] x_werte_1 = {p_middle.x, p_middle.x + (int) (radius / 4 * Math.sin(Math.PI * 2.0 * (i + 1) / (cellAmount))), p_middle.x + (int) (radius / 4 * Math.sin(Math.PI * 2.0 * (i) / (cellAmount)))};
            int[] y_werte_1 = {p_middle.y, p_middle.y - (int) (radius / 4 * Math.cos(Math.PI * 2.0 * (i + 1) / (cellAmount))), p_middle.y - (int) (radius / 4 * Math.cos(Math.PI * 2.0 * (i) / (cellAmount)))};
            int[] x_werte_2 = {p_middle.x, p_middle.x + (int) (radius * Math.sin(Math.PI * 2.0 * (i + 1) / (cellAmount))), p_middle.x + (int) (radius * Math.sin(Math.PI * 2.0 * (i) / (cellAmount)))};
            int[] y_werte_2 = {p_middle.y, p_middle.y - (int) (radius * Math.cos(Math.PI * 2.0 * (i + 1) / (cellAmount))), p_middle.y - (int) (radius * Math.cos(Math.PI * 2.0 * (i) / (cellAmount)))};
            int[] x_werte_3 = {p_middle.x, p_middle.x + (int) (1.3 * radius * Math.sin(Math.PI * 2.0 * (i + 1) / (cellAmount))), p_middle.x + (int) (radius * 1.3 * Math.sin(Math.PI * 2.0 * (i) / (cellAmount)))};
            int[] y_werte_3 = {p_middle.y, p_middle.y - (int) (1.3 * radius * Math.cos(Math.PI * 2.0 * (i + 1) / (cellAmount))), p_middle.y - (int) (radius * 1.3 * Math.cos(Math.PI * 2.0 * (i) / (cellAmount)))};
            g2.fillPolygon(x_werte_1, y_werte_1, 3);
            g2.setColor(Color.black);
            if (clicked) {
                if (contains(x_werte_2, y_werte_2, mousePosition)) {
                    Segment.setSelected_index(i);
                    clicked = false;
                }
            }
            if (i == Segment.getSelected_index()) {
                if (Segment.getSelected_index() != selectedIndexPrev)
                    ringbufferEditPanel.update(DataContainer.getSegments().get(i));
                g2.setColor(Color.GRAY);
                g2.fillPolygon(x_werte_3, y_werte_3, 3);
                if (DataContainer.getSegments().get(i).isLocked()) {
                    g2.setColor(Color.red);
                } else {
                    g2.setColor(Color.green);
                }
                g2.fillPolygon(x_werte_1, y_werte_1, 3);
                g2.setColor(Color.black);
                g2.drawPolygon(x_werte_3, y_werte_3, 3);
            } else if (contains(x_werte_2, y_werte_2, mousePosition)) {
                g2.setColor(Color.white);
                g2.fillPolygon(x_werte_3, y_werte_3, 3);
                g2.setColor(Color.black);
                g2.drawPolygon(x_werte_3, y_werte_3, 3);
            } else {
                g2.drawLine(p_middle.x, p_middle.y, p_middle.x + (int) (radius * Math.sin(Math.PI * 2.0 * (i + 1) / (cellAmount))), p_middle.y - (int) (radius * Math.cos(Math.PI * 2.0 * (i + 1) / (cellAmount))));
            }
        }

        if (clicked) {
            clicked = false;
            Segment.setSelected_index(-1);
        }
        g.drawLine(p_middle.x, p_middle.y, p_middle.x, p_middle.y - radius);
        selectedIndexPrev = Segment.getSelected_index();
    }

    private boolean contains(int[] x, int[] y, Point p) {
        int i, j;
        boolean result = false;
        for (i = 0, j = x.length - 1; i < x.length; j = i++) {
            if ((y[i] > p.y) != (y[j] > p.y) &&
                    (p.x < (x[j] - x[i]) * (p.y - y[i]) / (y[j] - y[i]) + x[i])) {
                result = !result;
            }
        }
        return result;
    }

}
