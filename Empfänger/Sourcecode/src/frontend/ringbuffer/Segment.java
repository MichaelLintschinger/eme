package frontend.ringbuffer;

import java.util.Arrays;

public class Segment {
    private boolean locked;
    private double[] adc = null;
    private int index;
    private static int length = 0, maxADC;
    private static int selected_index = -1;

    public Segment() {
        index = length;
        length++;
    }

    public static int getLength() {
        return length;
    }

    public static void setLength(int length) {
        Segment.length = length;
    }

    public static int getSelected_index() {
        return selected_index;
    }

    public static void setSelected_index(int selected_index) {
        Segment.selected_index = selected_index;
    }

    public static int getMaxADC() {
        return maxADC;
    }

    public static void setMaxADC(int maxADC) {
        Segment.maxADC = maxADC;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public double[] getAdc() {
        return adc;
    }

    public double getAdc(int index) {
        try {
            return adc[index];
        } catch (NullPointerException e) {
            adc = new double[0];
        } catch (Exception e) {
            double[] adcnew = new double[adc.length + 1];
            if (adc.length >= 0) System.arraycopy(adc, 0, adcnew, 0, adc.length);
            adcnew[adc.length] = -1;
            adc = adcnew;
        }
        return getAdc(index);
    }

    public void setAdc(double[] adc) {
        this.adc = adc;
        maxADC = Math.max(adc.length, maxADC);
    }

    public void setAdc(int index, double value) {
        double[] newadc = new double[index + 1];
        try {
            adc[index] = value;
            return;
        } catch (NullPointerException e) {
            newadc[index] = value;
        } catch (ArrayIndexOutOfBoundsException e) {
            for (int i = 0; i < newadc.length; i++) {
                if (i < adc.length) {
                    newadc[i] = adc[i];
                }
                if (i == index)
                    newadc[i] = value;
            }
        }
        adc = newadc;
    }

    public String toString() {
        try {
            return Arrays.toString(adc);
        } catch (NullPointerException e) {
            return "null";
        }
    }
}
