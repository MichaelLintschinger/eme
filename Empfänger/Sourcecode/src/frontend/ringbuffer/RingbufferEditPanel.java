package frontend.ringbuffer;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class RingbufferEditPanel extends JPanel {

    private ArrayList<JTextField> adc = new ArrayList<>();

    public RingbufferEditPanel(int width) {
        setBackground(Color.white);
        setLayout(null);
        int margin = 30;
        setSize(width, margin);
    }

    public void update(Segment segment) {
        removeAll();
        adc.clear();
        int amount = 0;
        try {
            amount = segment.getAdc().length;
        } catch (Exception e) {

        }
        setSize(getWidth(), amount * 30);
        if (amount == 0)
            setSize(getWidth(), 30);
        int i = 0;
        do {
            JTextField f = new JTextField();
            f.setBounds(0, i * 30, getWidth(), 20);
            if (amount == 0) {
                f.setText("-");
            } else {
                f.setText("" + segment.getAdc()[i]);
            }
            f.setEditable(false);
            adc.add(f);
            add(adc.get(adc.size() - 1));
            i++;
        } while (i < amount);
    }
}
