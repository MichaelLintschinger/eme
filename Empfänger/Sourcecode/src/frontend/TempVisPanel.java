package frontend;

import backend.Constants;
import backend.DataContainer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

interface change {
    void indexChanged(int newIndex);

    void measureChanged(int index);
}

public abstract class TempVisPanel extends JPanel implements change {

    private int indexADC = 0, x_measure = 0, offsetData = 0;
    private final int margin = 15;
    private boolean pressed = false;

    public TempVisPanel() {
        setFocusable(true);
        addMouseWheelListener(e -> {
            indexADC += (e.getUnitsToScroll() > 0) ? -1 : 1;
            indexADC = (indexADC < 0) ? 0 : indexADC;
            repaint();
            indexChanged(indexADC);
        });
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                pressed = true;
                repaint();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                pressed = false;
            }
        });
    }

    int amountData = 24, qx_measurePrev = -1;
    double value_measurePrev = 0;


    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.clearRect(0, 0, getWidth(), getHeight());
        if (pressed)
            x_measure = MouseInfo.getPointerInfo().getLocation().x - getLocationOnScreen().x - 5;
        int size = DataContainer.getSegments().size() - offsetData;

        //GRID
        g2.setColor(Color.BLACK);
        g2.drawLine(margin, margin, margin, getHeight() - margin / 2);
        g2.drawLine(margin / 2, getHeight() - margin, getWidth() - margin, getHeight() - margin);

        //MEASURE
        g2.setColor(Color.blue);
        double width = (getWidth() - margin * 2.0) * 1.0 / (Math.min(size, amountData - 1));
        int indexVis = (int) ((x_measure) / width);
        indexVis = Math.min(Math.max(indexVis, 0), amountData - 1);
        int qx_measure = (int) (margin + indexVis * width);
        qx_measure = Math.min(qx_measure, getWidth() - margin);
        g2.drawLine(qx_measure, 0, qx_measure, getHeight());
        if (qx_measure != qx_measurePrev) {
            measureChanged(size - amountData + indexVis);
        }
        try {
            if (value_measurePrev != DataContainer.getSegments().get((int) (((x_measure) / width) < 0 ? 0 : (x_measure) / width)).getAdc()[indexADC]) {
                measureChanged(size - amountData + indexVis);
            }
        } catch (Exception e) {
        }

        //max
        if (DataContainer.getSegments().size() == 0) return;
        double max_y = 0, min_y = DataContainer.getSegments().get(size - 1).getAdc(indexADC);
        int i = size - amountData;
        i = Math.max(i, 0);
        for (; i < size; i++) {
            try {
                max_y = Math.max(max_y, DataContainer.getSegments().get(i).getAdc()[indexADC]);
                min_y = Math.min(min_y, DataContainer.getSegments().get(i).getAdc()[indexADC]);
            } catch (Exception e) {

            }
        }

        //DATA
        min_y = (min_y == max_y) ? min_y - 1 : min_y;
        double height = max_y - min_y;
        g2.setColor(Color.red);
        i = size - amountData + 1;
        i = Math.max(i, 1);
        for (; i < size; i++) {
            try {
                double value[] = {DataContainer.getSegments().get(i - 1).getAdc()[indexADC],
                        DataContainer.getSegments().get(i).getAdc()[indexADC]};

                int x1 = (int) (margin + (i + amountData - size - 1) * width),
                        y1 = getHeight() - margin - (int) ((getHeight() - 2 * margin) * ((value[0] - min_y) / height)),
                        x2 = (int) (margin + (i + amountData - size) * width),
                        y2 = getHeight() - margin - (int) ((getHeight() - 2 * margin) * ((value[1] - min_y) / height));

                //System.out.println(x1 + "," + y1 + "," + x2 + "," + y2);

                g2.drawLine(x1, y1, x2, y2);

            } catch (NullPointerException e) {
                //System.out.println(i+","+DataContainer.getSegments().size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        qx_measurePrev = qx_measure;
        try {
            value_measurePrev = DataContainer.getSegments().get(indexVis).getAdc()[indexADC];
        } catch (Exception e) {
        }
    }

    public int getIndexADC() {
        return indexADC;
    }

    public boolean isPressed() {
        return pressed;
    }

    public int getOffsetData() {
        return offsetData;
    }

    public void setOffsetData(int offsetData) {
        this.offsetData = (offsetData + amountData < DataContainer.getSegments().size()) ? offsetData : DataContainer.getSegments().size() -1-amountData;
        this.offsetData = Math.max(this.offsetData, 0);
        System.out.println(this.offsetData);
    }

    public int getAmountData() {
        return amountData;
    }

    public TempVisPanel setAmountData(int amountData) {
        this.amountData = (offsetData + amountData < DataContainer.getSegments().size() ) ? amountData : DataContainer.getSegments().size()-1-offsetData ;
        this.amountData = Math.max(3, this.amountData);
        System.out.println(this.amountData);
        return this;
    }
}