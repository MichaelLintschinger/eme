package frontend.console;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

interface i_console {
    void process_command(String command);
}

public class Console extends JPanel implements i_console {
    TextField tf_eingabe = new TextField();
    TextArea tf_ausgabe = new TextArea();
    String prefix = "/";
    int index_history = 0;
    public ArrayList<String> help = new ArrayList<>(), history = new ArrayList<>();

    public Console() {
        setLayout(null);
        history.add("");
        addHelp("clear : (clears Console)");

        tf_eingabe.addActionListener(e -> {
            console_write("YOU", tf_eingabe.getText());
            history.add(tf_eingabe.getText());
            if (history.get(0).equals("")) history.remove(0);
            if (tf_eingabe.getText().split("")[0].equals(prefix)) {
                process_command(tf_eingabe.getText().substring(1));
            }
            tf_eingabe.setText("");
            index_history = history.size();
        });

        tf_eingabe.addKeyListener(new KeyAdapter() {


            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.VK_UP) {
                    index_history = Math.max(index_history - 1, 0);
                    tf_eingabe.setText(history.get(index_history));
                } else if (keyEvent.getKeyCode() == KeyEvent.VK_DOWN) {
                    index_history = Math.min(index_history + 1, history.size() - 1);
                    tf_eingabe.setText(history.get(index_history));
                }
            }


        });

        tf_ausgabe.setEditable(false);

        add(getTf_eingabe());
        add(getTf_ausgabe());

        setVisible(true);

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        getTf_ausgabe();
        getTf_eingabe();
    }

    public synchronized void console_write(String from, String msg) {
        if (msg.equals("")) return;
        if (from.equals("")) {
            tf_ausgabe.setText(tf_ausgabe.getText() + "\n" + msg);
            return;
        }
        tf_ausgabe.setText(tf_ausgabe.getText() + "\n" + from + " : '" + msg + "'");
    }

    public void clear() {
        tf_ausgabe.setText("");
    }

    public void process_command(String command) {
        String[] commands = command.split(" ");

        switch (commands[0]) {
            case "help": {
                for (String helper : help) {
                    console_write("", helper);
                }
                break;
            }
            case "clear": {
                clear();
                break;
            }
        }
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public TextField getTf_eingabe() {
        tf_eingabe.setBounds(0, 0, getWidth(), 30);
        return tf_eingabe;
    }

    public TextArea getTf_ausgabe() {
        tf_ausgabe.setBounds(0, 40, getWidth(), getHeight() - 40);
        return tf_ausgabe;
    }

    public void addHelp(String help) {
        this.help.add(help);
    }
}
