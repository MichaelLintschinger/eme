package frontend;

import backend.RxTx.SerielleSchnittstelle;
import backend.db.*;
import backend.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Layer0 {
    private static ArrayList input;
    private static boolean ready_to_read = false;
    private static SerielleSchnittstelle serielle_schnittstelle;
    private static SQLmanager SQLmanager = new SQLmanager(Constants.db_url + Constants.db_name, Constants.username, Constants.password);
    private static boolean testing = false;

    public static void main(String[] args) {
        //TODO: restart
        if (args.length < 2 && !args[0].equals("port")) {
            args = new String[]{args[0], "", "no_simulate"};
        }
        if (args.length < 2) {
            System.err.println("wrong param : { port/test , [port], no_simulate/simulate }");
            System.exit(0);
        }
        if (args.length < 3) {
            if (args[0].equals("port")) {
                args = new String[]{args[0], args[1], "no_simulate"};
            }
        }
        testing = args[0].equals("test");
        if (testing) {
            args = new String[]{args[0], "", Constants.arg_simulate};
        }
        input = new ArrayList();
        String[] finalArgs = args;
        if (testing) {
            new Thread(() -> {
                while (true) {//READING
                    String inputstr = "";
                    try {
                        byte[] b = new byte[System.in.available()];
                        System.in.read(b);
                        inputstr = new String(b);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (inputstr.length() > 0) {
                        System.out.println(inputstr);
                        for (char c : inputstr.toCharArray()) {
                            Logger.append("" + c);
                            SQLmanager.write(LocalDateTime.now(), (byte) c, false, finalArgs[2].equals(Constants.arg_simulate));
                            setSQLBit();
                            if (c != (char) Constants.end_of_command)
                                input.add(c);
                        }
                        ready_to_read = true;
                    }
                }
            }).start();
        } else {
            serielle_schnittstelle = new SerielleSchnittstelle(args[1]) {
                @Override
                public void read(byte read) {
                    Logger.append("" + (char) read);
                    SQLmanager.write(LocalDateTime.now(), read, false, finalArgs[2].equals(Constants.arg_simulate));
                    if (read == Constants.end_of_command) {
                        ready_to_read = true;
                    } else {
                        input.add((char) read);
                    }
                    setSQLBit();
                }

                @Override
                public void written(byte[] out) {
                    for (byte b : out) {
                        Logger.append("" + (char) b);
                        SQLmanager.write(LocalDateTime.now(), b, true, finalArgs[2].equals(Constants.arg_simulate));
                    }
                    setSQLBit();
                }
            };
        }
        new Thread(() -> {
            while (true) {
                try {
                    connectAndDataReceive();
                    if (testing)
                        write("end".getBytes());
                    Thread.sleep(1000 * 1 * 60);
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private static void setSQLBit() {
        SQLmanager.updateInterrupt(true);
    }

    public static void connectAndDataReceive() {
        try {
            if (check_connection()) {
                if (check_ChipID()) {
                    if (check_Firmware_version()) {
                        int data_amount = getDataAmount();
                        System.err.println(data_amount);
                        int i = 0;
                        i = i < 0 ? 0 : i;
                        for (; i < data_amount; i++) {
                            getData();
                            deleteData();
                        }
                    }
                }
                if (!check_EEPROM()) {
                    System.err.println("EEPROM damaged");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean check_connection() throws IOException {
        write(new byte[]{Constants.check_connection, Constants.end_of_command});
        for (int i = 0; i < 50; i++) {//wait for OK
            for (int j = 0; j < 100 && !ready_to_read; j++) {//break if recieved
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (ready_to_read) {
                if ((char) input.get(0) == Constants.OK && (char) input.get(1) == Constants.check_connection) {
                    input.clear();
                    ready_to_read = false;
                    System.out.println("Connection check : OK");
                    return true;
                }
            }
        }
        System.err.println("Connection check : Connection timed out");
        System.err.print("Response: '" + listToString(input) + "'");
        input.clear();
        return false;
    }

    public static boolean check_EEPROM() throws IOException {
        write(new byte[]{Constants.check_EEPROM, Constants.end_of_command});
        for (int i = 0; i < 50; i++) {//wait for OK
            for (int j = 0; j < 100 && !ready_to_read; j++) {//break if recieved
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (ready_to_read) {
                if ((char) input.get(1) == Constants.check_EEPROM) {
                    if ((char) input.get(0) == Constants.OK) {
                        System.out.println("EEPROM check : OK");
                        input.clear();
                        ready_to_read = false;
                        return true;
                    } else if ((char) input.get(0) == Constants.not_OK) {
                        System.err.println("EEPROM check : EEPROM is demaged");
                        input.clear();
                        ready_to_read = false;
                        return false;
                    }
                    System.out.println("EEPROM check : Strange return : '" + listToString(input) + "'");
                }
            }
        }
        System.err.println("EEPROM check : connection timed out");
        input.clear();
        return false;
    }

    public static boolean check_ChipID() throws IOException {
        write(new byte[]{Constants.check_chipID, Constants.end_of_command});
        for (int i = 0; i < 50; i++) {//wait for OK
            for (int j = 0; j < 100 && !ready_to_read; j++) {//break if recieved
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (ready_to_read) {
                if ((char) input.get(0) == Constants.OK && (char) input.get(1) == Constants.check_chipID) {
                    String chipID = "";
                    for (int j = 2; j < input.size(); j++) {
                        chipID += new String(new byte[]{(byte) (char) input.get(j)});
                    }
                    System.out.println("Chip ID : " + chipID);
                    input.clear();
                    ready_to_read = false;
                    return true;
                }
                System.out.println("Chip ID check : Strange return : '" + listToString(input) + "'");
                return false;
            }
        }
        System.err.println("Chip ID check : Connection timed out");
        input.clear();
        return false;
    }

    public static boolean check_Firmware_version() throws IOException {
        write(new byte[]{Constants.check_firmware_version, Constants.end_of_command});
        for (int i = 0; i < 50; i++) {//wait for OK
            for (int j = 0; j < 100 && !ready_to_read; j++) {//break if recieved
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (ready_to_read) {
                if ((char) input.get(0) == Constants.OK && (char) input.get(1) == Constants.check_firmware_version) {
                    String firmwareVersion = "";
                    for (int j = 2; j < input.size(); j++) {
                        firmwareVersion += new String(new byte[]{(byte) (char) input.get(j)});
                    }
                    System.out.println("FirmwareVersion : " + firmwareVersion);
                    input.clear();
                    ready_to_read = false;
                    return true;
                }
                System.out.println("Firmware version check : Strange return : '" + listToString(input) + "'");
            }
        }
        System.err.println("Firmware version check : Connection timed out");
        input.clear();
        return false;
    }

    public static int getDataAmount() throws IOException {
        write(new byte[]{Constants.get_data_amount, Constants.end_of_command});
        boolean tr = false;
        for (int i = 0; i < 50; i++) {//wait for OK
            for (int j = 0; j < 100 && !ready_to_read; j++) {//break if recieved
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (ready_to_read) {
                if ((char) input.get(0) == Constants.OK && (char) input.get(1) == Constants.get_data_amount) {

                    String amount = "", input = listToString(Layer0.input);
                    System.err.println(input);

                    amount = input.substring(2);

                    Layer0.input.clear();
                    ready_to_read = false;

                    try {
                        return Integer.parseInt(amount);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return -1;
                }
            }
        }
        System.err.println("Get amount of data : Connection timed out");
        input.clear();
        return -1;
    }

    public static boolean getData() throws IOException {

        byte[] written = {Constants.get_data, Constants.end_of_command};
        write(written);
        for (int i = 0; i < 50; i++) {//wait for OK
            for (int j = 0; j < 100 && !ready_to_read; j++) {//break if recieved
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (ready_to_read) {
                if ((char) input.get(0) == Constants.OK && (char) input.get(1) == Constants.get_data) {
                    String inputstr = listToString(input).substring(2);
                    System.out.println(inputstr);
                    for (int j = 0; j < inputstr.split("" + (char) Constants.break_character).length; j++) {
                        System.out.println("Value " + j + " : " + inputstr.split("" + (char) Constants.break_character)[j]);
                    }
                    System.out.println();
                    input.clear();
                    ready_to_read = false;
                    return true;
                }
            }
        }
        System.err.println("Get Data : Connection timed out");
        input.clear();
        return true;
    }

    public static boolean deleteData() throws IOException {

        byte[] written = {Constants.delete_data, Constants.end_of_command};
        write(written);
        for (int i = 0; i < 50; i++) {//wait for OK
            for (int j = 0; j < 100 && !ready_to_read; j++) {//break if recieved
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (ready_to_read) {
                if ((char) input.get(1) == Constants.delete_data) {
                    if (((char) input.get(0) != Constants.OK)) {
                        System.err.println("DELETE : strange return : " + listToString(input));
                        input.clear();
                        ready_to_read = false;
                        return false;
                    } else {
                        input.clear();
                        ready_to_read = false;
                        System.out.println("deleted last data sucessfully");
                        return true;
                    }
                }
            }
        }
        System.err.println("TIMEOUT : delete Timed out");
        input.clear();
        return false;
    }

    public static String listToString(ArrayList arrayList) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arrayList.size(); i++) {
            sb.append((char) arrayList.get(i));
        }
        return sb.toString();
    }

    private static void write(byte[] out) throws IOException {
        if (testing) {
            if (!new String(out).equals("end"))
                for (byte b : out) {
                    Logger.append("" + (char) b);
                    SQLmanager.write(LocalDateTime.now(), b, true, true);
                }
            System.out.print(Constants.test_console_indicator + new String(out));
        } else {
            serielle_schnittstelle.write(out);
        }
        setSQLBit();
    }

}
