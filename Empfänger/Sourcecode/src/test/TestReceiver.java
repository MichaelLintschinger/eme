package test;


import javax.swing.*;

import backend.Constants;
import frontend.console.Console;

import java.awt.event.*;
import java.io.*;

public class TestReceiver extends JFrame {
    Console myConsole;
    boolean scrolldown = true;
    String[] responses = {Constants.nullChar, Constants.nullChar, Constants.nullChar, Constants.nullChar, Constants.nullChar, Constants.nullChar, Constants.nullChar};
    Process proc;

    TestReceiver() {
        refreshSettings();
        refreshConfigFile();
        myConsole = new Console() {
            @Override
            public void process_command(String command) {
                super.process_command(command);
                //TODO
                String[] cmd = command.split(" ");
                switch (cmd[0]) {
                    case "start": {
                        try {
                            if (cmd.length < 2) {
                                myConsole.console_write("", "start test");
                                startTest("out/artifacts/Receiving_Endpoint/Receiving_Endpoint.jar");
                                break;
                            }
                            startTest(cmd[1]);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                    case Constants.scrolldown: {
                        scrolldown = Boolean.parseBoolean(cmd[1]);
                        if (scrolldown) {
                            myConsole.getTf_ausgabe().addTextListener(e -> {
                                getTf_ausgabe().setCaretPosition(getTf_ausgabe().getText().length());
                            });
                        } else {
                            myConsole.getTf_ausgabe().removeTextListener(getTf_ausgabe().getTextListeners()[0]);
                        }
                    }
                    break;
                    case "status": {
                        console_write(Constants.scrolldown, "" + scrolldown);
                    }
                    break;
                    case "r": {
                        int index = Integer.parseInt(cmd[1]);
                        if (cmd[2].equals("-c")) {
                            responses[index] = Constants.nullChar;
                            console_write("Console", "cleared answer " + cmd[1]);
                        } else {
                            responses[index] = command.substring(4);
                            responses[index] = responses[index].replaceAll("\\\\n", "\n");
                            console_write("Console", "set answer " + cmd[1] + " to '" + command.substring(4));
                        }
                    }
                    break;
                }
                refreshConfigFile();
            }
        };

        myConsole.addHelp("start ; start test");
        myConsole.addHelp("status ; returns settings information");
        myConsole.addHelp(Constants.scrolldown + " [true/false] ; scroll to bottom automatically");
        myConsole.addHelp("r (int) [-c] [text] ; replaces standard answer with [text]; [-c] clears answer");
        myConsole.getTf_ausgabe().addTextListener(e -> {
            myConsole.getTf_ausgabe().setCaretPosition(myConsole.getTf_ausgabe().getText().length());
        });
        add(myConsole);
        addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    System.err.println("destroy process");
                    proc.destroyForcibly();
                    System.err.println(proc.isAlive());
                } catch (Exception ee) {

                }
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
    }

    void refreshConfigFile() {
        try {
            File file = new File(Constants.logger_Path);
            file.mkdir();
            BufferedWriter writer = new BufferedWriter(new FileWriter(Constants.logger_Path + Constants.config_file));
            String out = Constants.scrolldown + ":" + scrolldown + "\n" +
                    "responses:";
            for (String str : responses) {
                out += str + ",";
            }
            out = out.substring(0, out.length() - 1);
            System.out.println("output:\n[" + out + "]");
            writer.write(out);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void refreshSettings() {
        try {
            File file = new File(Constants.logger_Path);
            file.mkdir();
            BufferedReader reader = new BufferedReader(new FileReader(Constants.logger_Path + Constants.config_file));
            String in = "", line;
            while ((line = reader.readLine()) != null) {
                in += line + "\n";
            }
            if (in.length() > 0) {
                in = in.substring(0, in.length() - 1);
                System.out.println("input:\n" + in);
                String[] in2 = in.split("\n");
                for (String str : in2) {
                    if (str.split(":")[0].equals(Constants.scrolldown)) {
                        scrolldown = Boolean.parseBoolean(str.split(":")[1]);
                    }
                }
                in = in.split("responses:")[1];
                responses = in.split(",");
            }
            System.out.println(responses.length);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    boolean running = true;
    int cnt = 0;

    void startTest(String program) throws IOException {
        System.out.println("start");
        try {
            if (proc.isAlive()) proc.destroyForcibly();
        } catch (NullPointerException e) {

        }
        proc = Runtime.getRuntime().exec("java -jar " + program + " test");
        // retreive the process output
        InputStream in = proc.getInputStream();
        InputStream err = proc.getErrorStream();
        OutputStream out = proc.getOutputStream();

        new Thread(() -> { //print error
            while (proc.isAlive()) {
                try {//TODO:
                    byte[] b = new byte[err.available()];
                    err.read(b);
                    running = (b.length > 0) || running;
                    System.err.print(new String(b));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(() -> {//process command
            while (proc.isAlive()) {
                try {
                    byte[] b = new byte[in.available()];
                    in.read(b);
                    running = (b.length > 0) || running;
                    String input = new String(b);
                    if (input.equals("reqend") || input.equals("end")) {
                        proc.destroy();
                        break;
                    }
                    if (input.length() == 0) continue;
                    if (input.substring(0, 1).equals("" + (char) Constants.OK)) continue;
                    myConsole.console_write("testprogram sent", input);
                    for (String input2 : input.split("\n")) {
                        if (input2.length() < 3) continue;
                        if (input2.substring(0, 3).equals(Constants.test_console_indicator)) {
                            input2 = input2.substring(3);
                            myConsole.console_write("processing command", input2);
                            byte[] b_out = null;
                            int index_command = -1;
                            switch (input2.split("")[0]) {
                                case "" + (char) Constants.check_connection: {
                                    index_command = 0;
                                    b_out = new byte[]{Constants.OK, Constants.check_connection, Constants.end_of_command};
                                }
                                break;
                                case "" + (char) Constants.check_EEPROM: {
                                    index_command = 1;
                                    b_out = new byte[]{Constants.OK, Constants.check_EEPROM, Constants.end_of_command};
                                }
                                break;
                                case "" + (char) Constants.check_chipID: {
                                    index_command = 2;
                                    b_out = ("" + (char) Constants.OK + (char) Constants.check_chipID + "0x82484" + (char) Constants.end_of_command).getBytes();
                                }
                                break;
                                case "" + (char) Constants.check_firmware_version: {
                                    index_command = 3;
                                    b_out = ("" + (char) Constants.OK + (char) Constants.check_firmware_version + "v0" + (char) Constants.end_of_command).getBytes();
                                }
                                break;
                                case "" + (char) Constants.get_data_amount: {
                                    index_command = 4;
                                    b_out = ("" + (char) Constants.OK + (char) Constants.get_data_amount + "12" + (char) Constants.end_of_command).getBytes();
                                }
                                break;
                                case "" + (char) Constants.get_data: {
                                    index_command = 5;
                                    b_out = ("" + (char) Constants.OK + (char) Constants.get_data + cnt + (char) Constants.break_character + (cnt + cnt % 5) + (char) Constants.break_character + (cnt / 3) + (char) Constants.end_of_command).getBytes();
                                    cnt++;
                                }
                                break;
                                case "" + (char) Constants.delete_data: {
                                    index_command = 6;
                                    b_out = ("" + (char) Constants.OK + (char) Constants.delete_data + (char) Constants.end_of_command).getBytes();
                                }
                                break;
                            }
                            if (b_out != null) {
                                b_out = (responses[index_command].equals(Constants.nullChar)) ? b_out : (responses[index_command]).getBytes();
                                out.write(b_out);
                                out.flush();
                                myConsole.console_write("response ", new String(b_out));
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("process running : " + proc.isAlive());
        }).start();
    }

    public static void main(String[] args) {
        TestReceiver testReceiver = new TestReceiver();
        testReceiver.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        testReceiver.setSize(800, 600);
        testReceiver.setLocationRelativeTo(null);
        testReceiver.setTitle("Receiver Test");
        testReceiver.setVisible(true);
    }

}
