package test;

import backend.RxTx.SerielleSchnittstelle;
import backend.Constants;

import java.io.IOException;
import java.util.*;

public class TestResponse {
    private static ArrayList input = new ArrayList();
    private final static String chipID = "0xAEF0E3", firmware_version = "1.0";
    private static int[][] adc = {{0, 65, 75, 22}, {1, 19, 73}, {2, 21, 67, 54}, {3, 16, 33, 88}, {4, 77, 78, 89}};


    public static void main(String[] args) {

        try {

            SerielleSchnittstelle serielle_schnittstelle = new SerielleSchnittstelle(args[0]) {
                @Override
                public void read(byte read) {


                    if (read == Constants.end_of_command) {

                        if ((byte) input.get(0) == Constants.check_connection) {
                            try {
                                write(new byte[]{Constants.OK, Constants.check_connection, Constants.end_of_command});
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else if ((byte) input.get(0) == Constants.check_EEPROM) {
                            try {
                                write(new byte[]{Constants.OK, Constants.check_EEPROM, Constants.end_of_command});
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else if ((byte) input.get(0) == Constants.check_chipID) {
                            try {


                                byte[] out = new byte[3 + chipID.length()];
                                for (int i = 2; i < out.length - 1; i++) {
                                    out[i] = (byte) chipID.toCharArray()[i - 2];
                                }

                                out[0] = Constants.OK;
                                out[1] = Constants.check_chipID;
                                out[out.length - 1] = Constants.end_of_command;

                                write(out);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else if ((byte) input.get(0) == Constants.check_firmware_version) {
                            try {
                                byte[] out = new byte[3 + firmware_version.length()];
                                for (int i = 2; i < out.length - 1; i++) {
                                    out[i] = (byte) firmware_version.toCharArray()[i - 2];
                                }

                                out[0] = Constants.OK;
                                out[1] = Constants.check_firmware_version;
                                out[out.length - 1] = Constants.end_of_command;

                                write(out);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else if ((byte) input.get(0) == Constants.get_data_amount) {

                            try {

                                int len = 0;
                                for (int i = 0; i < adc.length; i++) {
                                    if (adc[i][0] > -1) len++;
                                }

                                byte[] out = new byte[3 + ("" + len).length()];
                                for (int i = 2; i < out.length - 1; i++) {
                                    out[i] = (byte) ("" + len).toCharArray()[i - 2];
                                }


                                out[0] = Constants.OK;
                                out[1] = Constants.get_data_amount;
                                out[out.length - 1] = Constants.end_of_command;

                                write(out);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else if ((byte) input.get(0) == Constants.get_data) {

                            try {

                                String index_s = "";

                                for (int j = 1; j < input.size(); j++) {
                                    index_s += new String(new byte[]{(byte) input.get(j)});
                                }

                                int index = Integer.parseInt(index_s);

                                System.out.println();
                                System.out.println("array:");

                                for (int i = 0; i < adc.length; i++) {
                                    System.out.println(Arrays.toString(adc[i]));
                                }

                                int len = -1;
                                int[] adc_requested = {-1, 0, 0, 0};

                                for (int i = 0; i < adc.length; i++) {
                                    if (adc[i][0] > -1) len++;

                                    if (len == index) {
                                        adc_requested = adc[i];
                                        break;
                                    }

                                }

                                String out_s = new String(new byte[]{Constants.OK}) + new String(new byte[]{Constants.get_data}) + adc_requested[0];


                                for (int i = 1; i < adc_requested.length; i++) {
                                    out_s += new String(new byte[]{Constants.break_character}) + adc_requested[i];
                                }
                                out_s += new String(new byte[]{Constants.end_of_command});
                                write(out_s.getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else if ((byte) input.get(0) == Constants.delete_data) {

                            try {

                                String index_s = "";

                                for (int j = 1; j < input.size(); j++) {
                                    index_s += new String(new byte[]{(byte) input.get(j)});
                                }

                                int index = Integer.parseInt(index_s);

                                adc[index] = new int[]{-1, 0, 0, 0};

                                System.out.println("delete : " + index);
                                String out_s = new String(new byte[]{Constants.OK}) + new String(new byte[]{Constants.delete_data}) + index_s + new String(new byte[]{Constants.end_of_command});
                                write(out_s.getBytes());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        input.clear();
                    } else {
                        input.add(read);
                    }
                }

                @Override
                public void written(byte[] out) {

                }
            };
            while (true) ;
        } catch (NumberFormatException e) {
            System.err.println("wrong args: {String port, \"" + Constants.arg_simulate + "\"}");
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.err.println("missing args: {String port \"" + Constants.arg_simulate + "\"}");
        }


    }

}
