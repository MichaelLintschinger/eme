package test;

import backend.RxTx.*;
import com.fazecast.jSerialComm.SerialPort;

import java.io.IOException;

public class TestBackend {

    public static void main(String[] args) throws IOException {
        if (args.length > 1) {
            System.out.println(args[1]);
            if (args[1].equals("send")) {
                SendeLoop.main(new String[]{args[0], "all"});
            } else if (args[1].equals("receive")) {
                SerielleSchnittstelle s = new SerielleSchnittstelle(args[0]) {
                    @Override
                    public void read(byte read) {
                        System.out.print((char) (read));
                    }

                    @Override
                    public void written(byte[] out) {

                    }
                };
            } else if (args[1].equals("all")) {
                SerialPort port = SerialPort.getCommPort(args[0]);
                port.setBaudRate(9600);
                port.openPort();
                System.out.println(port.isOpen());

                new Thread(() -> {
                    while (true) {
                        try {
                            if (port.getInputStream().available() > 0) {
                                byte[] b = new byte[port.getInputStream().available()];
                                port.getInputStream().read(b);
                                for (byte bb : b)
                                    System.out.println((int) (char) bb);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                byte[] b = new byte[]{'a', '\n'};
                port.writeBytes(b,b.length);
            }
        } else {
            System.err.println("missing args: {String port, state(\"send\", \"receive\", \"all\")}");
        }
    }
}
